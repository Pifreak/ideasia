﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Ideasia")]
[assembly: AssemblyDescription("Music Visualizer that is also a Video Game.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("@optimuspi")]
[assembly: AssemblyProduct("Ideasia.Properties")]
[assembly: AssemblyCopyright("2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("53f6eaca-6288-4482-8933-ce6c14849275")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.1.0.1")]
[assembly: AssemblyFileVersion("0.1.0.1")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]

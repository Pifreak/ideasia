﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using Ideasia.Source.Timing;
using SFML.Graphics;
using SFML.System;
using System.Collections.Generic;
using Ideasia.Source.Entities;
using SFML.Window;


namespace Ideasia.Source.Main
{
    class MainWindow
    {
        #region - Fields -

        private static int MouseX = 0;
        private static int MouseY = 0;
        private static Timestep timestep;

        //Main Window that holds the game and everything
        private static RenderWindow _window;

        //Example object just for demonstration purposes TODO
        private static HashSet<Particle> particles = new HashSet<Particle>();

        #endregion

        #region - Methods -

        /// <summary>
        /// Main entry point for project Ideasia.
        /// </summary>
        static void Main()
        {
            // Set up our Main Window.
            InitWindow();

            // Set Up our game elements.
            InitGame();

            //Launch physics in the background until finished
            var physicsThread = new Thread(PhysicsLoop);
            physicsThread.Start();

            //Call main loop and run forever until finished
            MainWindowLoop();
        }

        /// <summary>
        /// PhysicsLoop will run several times per second in a timestep
        /// </summary>
        static void PhysicsLoop()
        {
            //Declare a timestep to run at a certain speed
            timestep = new Timestep(Ideasia.Properties.Settings.Default.WindowUpdateRate);

            //Never-ending loop is Window is open
            while (Window.IsOpen)
            {
                //Update timer to keep us in sync
                timestep.Update();

                //While it is time to advance, keep looping and checking.
                while (timestep.Check())
                {
                    AnimateParticles(timestep);

                }

                //Sleep to prevent eating CPU. 
                Thread.Sleep(1);

            }
            //Update Timestep 
        }

        
        private static void AnimateParticles(Timestep timestep)
        {
            


            lock (particles)
            {



                


                foreach (var particle in particles)
                {
                    bool collide = false;
                    //bounce off top / bottom of screen
                    if (particle.X > 0 &&
                        particle.X < Window.Size.X &&
                        (particle.Y + particle.Radius > Window.Size.Y || particle.Y - particle.Radius  < 0))
                    {
                        particle.YSpeed = -particle.YSpeed;// *0.5f;
                        particle.Animate();
                        particle.Radius *= 0.9f;
                        particle.R -=10;
                        particle.G -= 10;
                        particle.B -= 10;
                      //  particle.XSpeed *= 0.75f;
                        ////bounce crazy sideways
                        //if (timestep.Ticks() % 2 == 0)
                        //    particle.XSpeed = particle.YSpeed / 5;
                        //else
                        //    particle.XSpeed = -particle.YSpeed / 5 ;
                    }

                    

                    //bounce off left/right of screen
                    if (particle.Y > 0 &&
                        particle.Y < Window.Size.Y &&
                        (particle.X + particle.Radius > Window.Size.X || particle.X - particle.Radius < 0))
                    {
                        particle.XSpeed = -particle.XSpeed;// *0.5f;
                        particle.Animate();
                        particle.Radius *= 0.9f;
                        particle.R -= 10;
                        particle.G -= 10;
                        particle.B -= 10;
                        // particle.YSpeed *= 0.75f;
                        ////bounce crazy sideways
                        //if (timestep.Ticks() % 2 == 0)
                        //    particle.XSpeed = particle.YSpeed / 5;
                        //else
                        //    particle.XSpeed = -particle.YSpeed / 5 ;
                    }


                    particle.Animate();

                    if (particle.X > Window.Size.X + particle.Radius ||
                        particle.Y > Window.Size.Y + particle.Radius ||
                        particle.X < 0 - particle.Radius   ||
                        particle.Y < 0 - particle.Radius)
                    {
                        particle.Dead = true;
                    }


                    foreach (Particle p in particles)
                    {
                        if (Particle.CircleTest(particle, p) )
                        {

                            if (!collide) { 
                                if (particle.XSpeed > 0)
                                    particle.ColorG();
                                else if (particle.YSpeed > 0)
                                    particle.ColorB();
                                else
                                    particle.ColorR();
                            }

                            if (particle != p && particle.Dead == false && particle.Radius > p.Radius)
                            {
                                if (p.Radius < 1)
                                    p.Dead = true;

                                if (!collide)
                                particle.Radius += 1;


                                collide = true;
                                p.Radius--;
                                if (particle.R > 248 && particle.G > 248 && particle.B > 248)
                                {
                                    particle.Dead = true;
                                }
                            }
                           // particle.ColorA();

                        }
                    }


                }

                //remove dead particles
                particles.RemoveWhere(p => p.Dead);

            }
        }

        /// <summary>
        /// Initialize the Main Window which holds everything. Set dimensions and title. 
        /// </summary>
        static void InitWindow()
        {
            // Load our Main Window title and dimensions.
            var windowWidth = Ideasia.Properties.Settings.Default.WindowWidth;
            var windowHeight = Ideasia.Properties.Settings.Default.WindowHeight;
            var windowTitle = Ideasia.Properties.Resources.WindowTitle;

            // Initialize Main Window.
            Window = new RenderWindow(new SFML.Window.VideoMode(windowWidth, windowHeight, 8), windowTitle, style:Styles.Fullscreen);

            //Set Window Icon
            var icon = new Image(Ideasia.Properties.Resources.IconFilename);
            Window.SetIcon(icon.Size.X, icon.Size.Y, icon.Pixels);

            //Limit frame rate and use V-Sync
            Window.SetVerticalSyncEnabled(Convert.ToBoolean(Ideasia.Properties.Settings.Default.WindowVSyncEnabled));


            //Event Handlers
            Window.Closed += WindowClosed;
            Window.MouseMoved += MouseMoved;
        }

        private static void MouseMoved(object sender, MouseMoveEventArgs e)
        {
            if (timestep.Ticks() % 10 < 6)
                return;

            MouseX = e.X;
            MouseY = e.Y;

            //Sideways motion
            float inputA = 3000;
            float inputA2 = 1400;

            //Veritcal motion
            float inputB = 7000;
            float inputB2 = 3400;

            //Restrictor
            float inputC = 1800.0f;
            float inputC2 = 2400.0f;

            //Size & Color
            Color inputD = Color.Yellow;
            long inputD2 = (timestep.Ticks() % 6 + 1 );


            //Size
            switch (timestep.Ticks() % 4)
            {
                case 0:
                    inputD = Color.Magenta;
                    break;

               case 1:
                    inputD = Color.Green;
               break;

                case 2:
                    inputD = Color.Cyan;
                break;

            }


            float randXSpeed = (timestep.Ticks() % inputA - inputA2) / inputC + 4;
            float randYSpeed = (timestep.Ticks() % inputB - inputB2) / inputC2 + 4;

            lock (particles)
            {
                if (particles.Count < 100)
                {
                    particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed =  randXSpeed, YSpeed =  randYSpeed, Color = Color.White, Radius = inputD2 });
                    particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });
                    particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed =  randYSpeed, Color = Color.White, Radius = inputD2 });
                    particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed =  randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });

                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = randXSpeed, YSpeed = randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed = randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });

                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = randXSpeed, YSpeed = randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = -randXSpeed, YSpeed = randYSpeed, Color = Color.White, Radius = inputD2 });
                    //particles.Add(new Particle() { X = MouseX, Y = MouseY, XSpeed = randXSpeed, YSpeed = -randYSpeed, Color = Color.White, Radius = inputD2 });
                }
            }

        }

        /// <summary>
        /// Initialize the Game Elements and content.
        /// </summary>
        static void InitGame()
        {
            //Our first example game element TODO
            
        }


        /// <summary>
        /// WindowClosed is an event handler which allows us to exit the program upon the user's request.
        /// </summary>
        /// <param name="sender">RenderWindow who triggered the event.</param>
        /// <param name="eventArgs">Default required value.</param>
        private static void WindowClosed(object sender, EventArgs eventArgs)
        {
            //Close Window and break out of loops
            // Close the window when OnClose event is received
            var window = (RenderWindow)sender;
            window.Close();
        }

        /// <summary>
        /// Run main loop in a timestep
        /// </summary>
        static void MainWindowLoop()
        {
            //Make window active immediately before starting this loop
            Window.SetActive();

            //loop forever until user exists (handled by the event handler WindowClosed)
            while (Window.IsOpen)
            {
                //Events such as closing the window
                Window.DispatchEvents();

                //Draw graphics and refresh screen
                Graphics();
            }
        }

        static void Graphics()
        {
            Window.Clear();

            //foreach (Particle particle in particles)
            //    Window.Draw(particle.Circle());

            if (particles.Count > 0)
            {

                lock (particles)
                {
                    foreach (var particle in particles)
                    {
                        Window.Draw(particle.Circle());
                    }
                }
                
            }

            Window.Display();
        }

        #endregion

        #region - Properties -

        public static RenderWindow Window
        {
            get { return _window; }
            set { _window = value; }
        }

    

        #endregion

    }
}
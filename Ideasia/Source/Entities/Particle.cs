﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Ideasia.Source.Entities
{
    class Particle
    {
        #region - Fields -

        private CircleShape _circle;
        public Byte R, G, B;

        private Color _color;

        #endregion

        #region - Methods -

        public Particle()
        {
            X = 0;
            Y = 0;
            XSpeed = 0;
            YSpeed = 0;
            Radius = 1;
            TicksRemaining = 1000;
            R = G = B = 0;
            _circle = new CircleShape
            {
                Radius = Radius,
                Position = new Vector2f(X, Y),
                FillColor = Color.White,
            }; 

        }

        #endregion

        #region - Properties -

        public float X { get; set; }
        public float Y { get; set; }
        public float XSpeed { get; set; }
        public float YSpeed { get; set; }
        public float Radius { get; set; }
        public float TicksRemaining { get; set; }
        public Color Color { get { return _color; } set { _color = value; } }
        public bool Dead { get; set; }
        
        public void Animate()
        {
            X += XSpeed;
            Y += YSpeed;
            //YSpeed -= 0.09f;
            //XSpeed -= 0.03f;
        }

        public void ColorR()
        {
            if (R < 250)
            R += 2;

            //if (B > 1)
            //    B -= 1;
            
            //if (G > 1)
            //    G -= 1;


        }
        public void ColorA()
        {
            _color.A++;// -= Convert.ToByte(1);
        }
        public void ColorG()
        {
            if(G < 250)
            G += 2;

            //if (B > 1)
            //    B -= 1;

            //if (R > 1)
            //    R -= 1;
        }
        public void ColorB()
        {
            if (B < 249)
            B += 2;

            //if (R > 1)
            //    R -= 1;

            //if (G > 1)
            //    G -= 1;

        }

        public static bool CircleTest(Particle Object1, Particle Object2)
        {
            float xd = Object1.X - Object2.X;
            float yd = Object1.Y - Object2.Y;

            return Math.Sqrt(xd * xd + yd * yd) <= Object1.Radius + Object2.Radius;
        }
        
        public CircleShape Circle()
        {
            _circle.Position = new Vector2f(X, Y);
            _circle.FillColor = new Color(R, G, B, 230) ;
            _circle.Radius = Radius;
            return _circle;
        }

        #endregion

    }
}

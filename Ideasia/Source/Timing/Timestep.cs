﻿using System;
using SFML.System;

namespace Ideasia.Source.Timing
{
    class Timestep
    {
        #region - Fields -

        private bool isReady;
        private readonly double frameInterval;
        private double accumulator;

        private Time lastTime;
        private Time thisTime;
        private Clock clock;


        #endregion

        #region - Methods -


        public Timestep(double updatesPerSecond)
        {
            //Initialize clock
            clock = new Clock();

            //Restart the clock
            clock.Restart();

            //Get interval for each frame, 1000000.0 microseconds per second
            frameInterval = 1000000.0 / updatesPerSecond;

            //Get the initial current time
            thisTime = clock.ElapsedTime;
            lastTime = Time.Zero;

            //Set the accumulator to 0
            accumulator = 0;
        }


        public void Update()
        {
            // Grab the current time
            thisTime = clock.ElapsedTime;

            // Calculate the difference between the current and last times.
            accumulator += (thisTime.AsMicroseconds() - lastTime.AsMicroseconds());

            // Preserve the time for next Update()
            lastTime = thisTime;
        }

        public bool Check()
        {
            // Return false if we are still waiting.
            if (!(accumulator >= frameInterval)) 
                return false;

            // Essentially count down until it's time to call Update()
            accumulator -= frameInterval;

            // Return true because it is time for Update to run.
            return true;
        }

        /// <summary>
        /// Return a tick count from one of the timers.
        /// </summary>
        /// <returns></returns>
        public long Ticks()
        {
            return lastTime.AsMicroseconds();
        }

        #endregion

    }
}
